const prompt = require("prompt-sync")();


/*
Ex 1. Escreva um algoritmo que leia um número digitado 
pelo usuário e mostre a mensagem “Número maior do
que 10! ”, caso este número seja maior, ou “Número menor 
ou igual a 10! ”, caso este número seja menor ou igual.
*/
function rodarExercicio01(){
    let numero = prompt("Informe um número: ");
    if (numero > 10) {
        console.log("Número maior do que 10!");
    } else {
        console.log("Número menor ou igual a 10!");
    }
}

//rodarExercicio01();

/*
Ex 2. Escreva um algoritmo que leia dois números 
digitados pelo usuário e exiba o resultado da sua soma.
*/
function rodarExercicio02(){
    let primeiroNumero = Number(prompt("Informe o primeiro número: "));
    let segundoNumero = parseInt(prompt("Informe o segundo número: ")); 
    const resultado = primeiroNumero + segundoNumero;
    console.log("Resultado: " + resultado);
}

//rodarExercicio02();

